<?php
namespace api\modules\v1\models\auth;

use Yii;
use yii\base\Model;
use common\models\RepRekeningTunda;

/**
 * Login form
 */
class PhoneForm extends Model
{
    public $phone_number;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone_number'], 'required'],
            
        ];
    }
}