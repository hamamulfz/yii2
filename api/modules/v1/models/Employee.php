<?php

namespace api\modules\v1\controllers;

// use yii\db\schema\CDbExpression;
use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $name
 * @property string $birthday
 * @property string $email
 * @property string $photo
 * @property int $created_at
 * @property int $updated_at
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['birthday'], 'safe'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['email', 'photo'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['photo'], 'unique'],
        //     array('updated_at','default',
        //       'value'=>new \yii\db\Expression('NOW()'),
        //       'on'=>'update'),
        // array('created_at, updated_at','default',
        //       'value'=>new \yii\db\Expression('NOW()'),
        //       'on'=>'insert')
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birthday' => 'Birthday',
            'email' => 'Email',
            'photo' => 'Photo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
