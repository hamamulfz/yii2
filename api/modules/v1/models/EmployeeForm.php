<?php
namespace api\models;

use Yii;
use yii\base\Model;
use api\models\Employee;

/**
 * Login form
 */
class EmployeeForm extends Model
{
    public $id;
    public $name;
    public $birthday;
    public $email;
    public $photo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['name','email' ], 'required'],
            ['id', 'number'],
            [['birthday', 'photo'], 'string']
            
            // rememberMe must be a boolean value
            // ['email', 'email', 'unique'],
            // ['birthday', 'date'],
            
        ];
    }
}