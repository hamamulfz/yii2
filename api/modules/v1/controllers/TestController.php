<?php

namespace api\modules\v1\controllers;

use Yii ;
use yii\rest\Controller;
use yii\web\Response;

use common\models\LoginForm;

class TestController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actionIndex()
    {
        // $examples = Example::find()->all();
        return [
            'data' => 'test index',
        ];
    }


    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {

            return  [
                'status' => 'success',
                'message' => 'login success',
                'data' => [
                    'id' => $model->user->id,
                    'username' => $model->user->username,
                    // token diambil dari field auth_key
                    'token' => $model->user->auth_key,
                ]
            ];
        } else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                'data' => '',
            ];
        }
        // return $response;
    }

    protected function verbs()
    {
        return [
            'login' => ['POST'],
            'index' => ['GET'],
        ];
    }
}
