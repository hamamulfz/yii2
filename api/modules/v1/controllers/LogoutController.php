<?php
namespace api\modules\v1\controllers;
use Yii;

use common\models\User;
 
class LogoutController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behavior = parent::behaviors();
        $behavior['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;
        $behavior['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
        ];
        $behavior['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        return $behavior;
    }
        // public function actionIndex(){
        //     return [ 'status' => 'sukses',
        //     'data' => 'abc'
        //     ];
        // }

        public function actionIndex(){
            $userId = Yii::$app->user->id; 
           
            $model = User::findOne($userId);
            // var_dump($model);
            // $model->auth_key = null;
            $model->generateAuthKey();
            $model->save();
            return [ 
            'success' => true,
            // 'data' => 'You are already logout'
            ];
        }
}
