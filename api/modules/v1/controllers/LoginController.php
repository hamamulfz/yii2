<?php
namespace api\modules\v1\controllers;
use Yii;

use common\models\User;
use yii\rest\Controller;
use yii\web\Response;

use common\models\LoginForm;

class LoginController extends Controller
{

    // public $modelClass = 'api\models\Artikel';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        
        $behaviors['corsFilter'] = [
          'class' => \yii\filters\Cors::className(),
      ];
        return $behaviors;
    }

    public function actionIndex(){
      $model = new LoginForm();
      if ($model->load(Yii::$app->request->post(),'') && $model->login()) {
  
        return  [
          'success' => true,
          // 'message' => 'login success',
          'data' => [
              'id' => $model->user->id,
              'username' => $model->user->username,
              // token diambil dari field auth_key
              'jwt_token' => $model->user->auth_key,
          ]
        ];     
      } else {
        return [
                  'success' => false,
                  'error' => 200,
                  'message' => "invalid username or password",
                  // $model->errors,
                  // 'data' => '',
                ];
      }
      // return $response;
    }
  
    protected function verbs()
    {
      return [
          'login' => ['POST'],
      ];
    }
}
