<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use api\modules\v1\models\RekTundaForm;
use common\models\RepRekeningTunda;

class RekeningTundaController extends Controller
{

    // public $modelClass = 'api\models\Artikel';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    
    public function actionIndex()
    {
        $model = new RekTundaForm();
        $model->load(Yii::$app->request->post(),'');
        // var_dump($model->tahun_bulan);
        // die;
        if($model->validate()){
            $data = RepRekeningTunda::find()->where(['tahun_bulan' => $model->tahun_bulan])->all();
            // var_dump($data);
            if($data ){
                    return ['status'=>'success',
                        'data' => $data,
                        // 'bonus' => new \yii\db\Expression('NOW()')
                    ];
            } else {
                return ['status'=>'fail',
                        'message' => 'no data found',
                        ];
            }
        }else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                // 'data' => '',
            ];
        }
    }
}