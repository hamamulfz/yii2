<?php

namespace api\modules\v1\controllers;

use Yii;

use api\modules\v1\models\auth\PhoneForm;
use common\models\User;
use yii\rest\Controller;
use yii\web\Response;

use frontend\models\SignupForm;
use common\models\LoginForm;

class AuthController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'register' => ['POST'],
            'set-otp' => ['POST'],

            'verify-login' => ['POST'],
            'verify-register' => ['POST'],
        ];
    }

    // if user doesnot exist, create one
    // if user exist, for the first time, otp change status, send token
    // if user exist and status active, send token

    public function actionSignup()
    {

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            // Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');

            return [
                'status' => 'success',
                'data' => 'user berhasil dibuat, otp',
                // 'bonus' => new \yii\db\Expression('NOW()')
            ];

            // return $this->goHome();
        } else {
            return [
                'status' => 'fail',
                'message' => $model->errors,
            ];
        }
    }

    public function actionRegister()
    {
        $model = new PhoneForm();
        $model->load(Yii::$app->request->post(), '');
        // var_dump($model);
        if ($model->validate()) {


            $check =  User::findOne(['username' => $model->phone_number]);
            if ($check) {
                return [
                    'status' => 'failed',
                    'data' => 'user already exist',
                    // 'bonus' => new \yii\db\Expression('NOW()')
                ];
            } else {
                $random = rand(100000, 999999);
                var_dump($random);

                $user = new User();
                $user->username = $model->phone_number;
                // $user->email = $this->email;
                $user->setPassword($random);
                $user->generateAuthKey();
                $user->generateEmailVerificationToken();

                // $model_employee->created_at = new \yii\db\Expression('NOW()');

                if ($user->validate() && $user->save()) {
                    return [
                        'status' => 'success',
                        'data' => 'user berhasil dibuat, otp' . $random,
                        // 'bonus' => new \yii\db\Expression('NOW()')
                    ];
                } else {
                    return [
                        'status' => 'fail',
                        'message' => $user->errors,
                    ];
                }
            }
        } else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                // 'data' => '',
            ];
        }
    }

    public function actionSetOtp()
    {
        $model = new PhoneForm();
        $model->load(Yii::$app->request->post(), '');
        $random = rand(100000, 999999);
        // var_dump($model);
        if ($model->validate()) {
            $model_employee =   User::findOne(['username' => $model->phone_number]);
            $model_employee->password = $random;
            $items = strval($random);
            var_dump($items);

            // $model_employee->created_at = new \yii\db\Expression('NOW()');

            if ($model_employee->validate() && $model_employee->save()) {
                return [
                    'status' => 'success',
                    'data' => 'OTP has changed to $items ' . $items,
                    // 'bonus' => new \yii\db\Expression('NOW()')

                ];
            } else {
                return [
                    'status' => 'fail',
                    'message' => $model_employee->errors,
                ];
            }
        } else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                // 'data' => '',
            ];
        }
    }

    public function actionVerifyLogin()
    {
        //if active

        //if not active yet
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login() && $model->validate()) {

            return  [
                'status' => 'success',
                'message' => 'login success',
                'data' => [
                    'id' => $model->user->id,
                    'username' => $model->user->username,
                    // token diambil dari field auth_key
                    'token' => $model->user->auth_key,
                ]
            ];
        } else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                'data' => '',
            ];
        }
    }


    public function actionVerifyRegister()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return [
                'status' => 'error',
                'message' => $model,
                'data' => '',
            ];
        } else {
            return [
                'status' => 'err',
                'message' => $model->errors,
                'data' => '',
            ];
        }
        die;

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {

            $check =  User::findOne(['username' => $model->phone_number]);
            if ($check->password === $model->password) {

                $model->user->status = User::STATUS_ACTIVE;
                if ($model->save()) {
                    return [
                        'status' => 'success',
                        'data' => 'OTP has changed',
                        // 'bonus' => new \yii\db\Expression('NOW()')

                    ];
                }
            } else {

                return  [
                    'status' => 'success',
                    'message' => 'login success',
                    'data' => [
                        'id' => $model->user->id,
                        'username' => $model->user->username,
                        // token diambil dari field auth_key
                        'token' => $model->user->auth_key,
                    ]
                ];
            }
            // if ($model->login()) { }
        } else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                'data' => '',
            ];
        }
    }


    public function actionIndex()
    {
        // $examples = Example::find()->all();
        return [
            'data' => 'test index',
        ];
    }
}
