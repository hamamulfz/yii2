<?php

namespace api\modules\v1\controllers;
use yii\rest\Controller;
use yii\web\Response;

class TestController extends Controller
{

    // public $modelClass = 'api\models\Artikel';
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    protected function verbs()
    {
       return [
           'index' => ['GET'],
       ];
    }

    public function actionIndex()
    {
        // $examples = Example::find()->all();
        return [
            'data'=>'test',
        ];
    }

    public function action()
    {
        // $examples = Example::find()->all();
        return [
            'data'=>'test',
        ];
    }
}
