<?php
namespace api\controllers;
use Yii;

use common\models\User;
use common\models\LoginForm;
 
class LoginController extends \yii\rest\Controller
{

  public function actionIndex(){
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post(),'') && $model->login()) {

      return  [
        'status' => 'success',
        'message' => 'login success',
        'data' => [
            'id' => $model->user->id,
            'username' => $model->user->username,
            // token diambil dari field auth_key
            'token' => $model->user->auth_key,
        ]
      ];     
    } else {
      return [
                'status' => 'error',
                'message' => $model->errors,
                'data' => '',
              ];
    }
    return $response;
  }

  protected function verbs()
  {
    return [
        'login' => ['POST'],
    ];
  }
  public function actionTest(){
      return [
                'status' => 'test',
                'message' => 'test',
                'data' => '',
              ];
    
  }

}
 
