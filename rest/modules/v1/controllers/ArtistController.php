<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Country;
use yii\rest\Controller;
use yii\web\Response;
/**
 * Artist Controller API
 *
 * @author Unknown
 */

class ArtistController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Country';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
 // Default actions
 // GET /artists: list all artists
 // HEAD /artists: show the overview information of artist listing
 // POST /artists: create a new artist
 // GET /artists/AU: return the details of the artist AU
 // HEAD /artists/AU: show the overview information of artist AU
 // PATCH /artists/AU: update the artist AU
 // PUT /artists/AU: update the artist AU
 // DELETE /artists/AU: delete the artist AU
 // OPTIONS /artists: show the supported verbs regarding endpoint /artists
 // OPTIONS /artists/AU: show the supported verbs regarding endpoint /artists/AU.

 public function actions()
 {
        $actions = parent::actions();
 // Possibility to unset default actions
        // unset($actions['index']);
        return $actions;
 }

 // Define custom actions
 // public function actionAlive()
 // {
 //     return new ActiveDataProvider([
 //         'query' => Proxy::find()->where(['Alive' => 1]),
 //         'pagination' => false,
 //     ]);
 // }
}