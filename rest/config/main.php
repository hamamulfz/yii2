<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
        'v2' => [
            'class' => 'app\api\modules\v2\Module',
        ],
        'gii' => 'yii\gii\Module',
    ],
    
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'parsers' => [
             'application/json' => 'yii\web\JsonParser',
            ]
           ],
           'user' => [
               'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => null,
           ],
           'urlManager' => [
            'enablePrettyUrl' => true,
            // 'enableStrictParsing' => true,
            'showScriptName' => false,
           ]
    ],
    'params' => $params,
];
