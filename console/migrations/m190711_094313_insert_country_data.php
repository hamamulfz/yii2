<?php

use yii\db\Migration;

/**
 * Class m190711_094313_insert_country_data
 */
class m190711_094313_insert_country_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('country', 
            [
            'id' => '1',
            'code' => 'AU',
            'name' => 'Australia',
            'population' => 18886000
            ]
        );

        $this->batchInsert('country',  [ 'id','code', 'name' , 'population'],
        [
            [ '2', 'BR', 'Brazil', 170115000 ],        
            [ '3', 'CA', 'Canada', 1147000 ],
            [ '4', 'CN', 'China', 1277558000],
            [ '5', 'DE','Germany',82164700],
            [ '6', 'FR','France',59225700 ],
            ['7', 'GB','United Kingdom',59623400],
            ['8', 'IN','India',1013662000],
            ['9', 'RU','Russia',146934000],
            ['10', 'US','United States',278357000]
        ]
    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190711_094313_insert_country_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190711_094313_insert_country_data cannot be reverted.\n";

        return false;
    }
    */
}
